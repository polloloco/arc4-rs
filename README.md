# arc4-rs

arc4-rs is a pure rust implementation of the [RC4](https://en.wikipedia.org/wiki/RC4) algorithm based on the [rust-crypto](https://github.com/DaGenix/rust-crypto/blob/master/src/rc4.rs) crate.

## License
This code is dual licensed under [MIT](https://choosealicense.com/licenses/mit/) and [Apache 2.0](https://choosealicense.com/licenses/apache-2.0/)