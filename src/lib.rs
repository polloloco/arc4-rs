// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

/*!
 * An implementation of the RC4 (also sometimes called ARC4) stream cipher. THIS IMPLEMENTATION IS
 * NOT A FIXED TIME IMPLEMENTATION.
 */

use bytes::Bytes;

const STATE_SIZE: usize = 256;

#[derive(Copy, Clone)]
pub struct Arc4 {
    i: u8,
    j: u8,
    state: [u8; STATE_SIZE],
}

impl Arc4 {
    fn create(key: &[u8], iterations: usize) -> Self {
        assert!(!key.is_empty() && key.len() <= STATE_SIZE);
        let mut arc4 = Arc4 {
            i: 0,
            j: 0,
            state: [0; STATE_SIZE],
        };
        for i in 0..STATE_SIZE {
            arc4.state[i] = i as u8;
        }

        for _ in 0..iterations {
            let mut j: u8 = 0;
            for i in 0..STATE_SIZE {
                j = j
                    .wrapping_add(arc4.state[i])
                    .wrapping_add(key[i % key.len()]);
                arc4.state.swap(i, j as usize);
            }
        }

        arc4
    }

    pub fn new(key: &[u8]) -> Self {
        Self::create(key, 1)
    }

    pub fn with_iterations(key: &[u8], iterations: usize) -> Self {
        Self::create(key, iterations)
    }

    fn next(&mut self) -> u8 {
        self.i = self.i.wrapping_add(1);
        self.j = self.j.wrapping_add(self.state[self.i as usize]);
        self.state.swap(self.i as usize, self.j as usize);
        self.state[(self.state[self.i as usize].wrapping_add(self.state[self.j as usize])) as usize]
    }

    fn encrypt_decrypt(&mut self, input: &[u8]) -> Bytes {
        input.iter().map(|&x| x ^ self.next()).collect()
    }

    fn encrypt_decrypt_inplace(&mut self, input: &mut [u8]) {
        for x in input.iter_mut() {
            *x ^= self.next();
        }
    }

    pub fn advance(&mut self, len: usize) {
        for _ in 0..len {
            self.next();
        }
    }

    pub fn encrypt(&mut self, input: &[u8]) -> Bytes {
        self.encrypt_decrypt(input)
    }

    pub fn decrypt(&mut self, input: &[u8]) -> Bytes {
        self.encrypt_decrypt(input)
    }

    pub fn encrypt_inplace(&mut self, input: &mut [u8]) {
        self.encrypt_decrypt_inplace(input)
    }

    pub fn decrypt_inplace(&mut self, input: &mut [u8]) {
        self.encrypt_decrypt_inplace(input)
    }
}

#[cfg(test)]
mod test {
    use super::Arc4;

    struct Test {
        key: &'static str,
        input: &'static str,
        output: &'static [u8],
    }

    const TESTS: &[Test] = &[
        Test {
            key: "Key",
            input: "Plaintext",
            output: &[0xBB, 0xF3, 0x16, 0xE8, 0xD9, 0x40, 0xAF, 0x0A, 0xD3],
        },
        Test {
            key: "Wiki",
            input: "pedia",
            output: &[0x10, 0x21, 0xBF, 0x04, 0x20],
        },
        Test {
            key: "Secret",
            input: "Attack at dawn",
            output: &[
                0x45, 0xA0, 0x1F, 0x64, 0x5F, 0xC3, 0x5B, 0x38, 0x35, 0x52, 0x54, 0x4B, 0x9B, 0xF5,
            ],
        },
    ];

    #[test]
    fn wikipedia_tests() {
        for test in TESTS.iter() {
            let mut rc4 = Arc4::new(test.key.as_bytes());
            let encrypted = rc4.encrypt(test.input.as_bytes());
            assert!(encrypted == test.output);
        }
    }
}
